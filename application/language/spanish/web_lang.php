<?php
/*******************************************************************************
********************************************************************************
**************************** TRADUCCION AL ESPAÑOL *****************************
********************************************************************************
********************************************************************************/
$lang['leng_pt'] = 'Portugués';
$lang['leng_es'] = 'Español';

$lang['leng_somos'] = 'AHORA SOMOS';

$lang['lbl_menu_contacto'] = 'Contacto';
$lang['lbl_tlt_config'] = 'Configuración avanzada';

$lang['lbl_tlt_compon'] = 'Componentes para Optimizar tu Quirófano';
$lang['lbl_tlt_pads'] = 'Pads para Optimizar tu Quirófano';

$lang['lbl_form_contacto'] = 'https://go.pardot.com/l/8232/2022-03-09/bz74pt';

$lang['lbl_ver_mas'] = 'Ver Más';
$lang['lbl_mas_info'] = 'Más información';
$lang['lbl_obtener'] = 'Obtener ahora';

$lang['lbl_cantidad'] = 'CANTIDAD';

$lang['lbl_tipos_procedimientos'] = 'Tipos de Procedimientos';
$lang['lbl_tipos_procedimientos_desc'] = 'TORI te recomendará los equipos y componentes esenciales para optimizar cada procedimiento quirúrgico.';
$lang['lbl_tipos_quirofanos'] = 'Tipos de Quirófanos';
$lang['lbl_tipos_quirofanos_desc'] = 'Selecciona el tipo de sala que deseas evaluar y TORI te brindará la solución quirúrgica que mejor responda a tu necesidad.';
$lang['lbl_soluciones_title'] = 'Soluciones Recomendadas';

$lang['lbl_componentes'] = 'COMPONENTES';
$lang['lbl_componentes_min'] = 'Componentes';
$lang['lbl_pads'] = 'PADS';
$lang['lbl_pads_min'] = 'Pads';
$lang['lbl_solicitar_mas_info'] = 'Solicitar más información';
$lang['lbl_posicion'] = 'Ver posicionamiento';

$lang['lbl_descargar'] = 'Descargar';
$lang['lbl_video_micro'] = 'Ver video Microclima';

$lang['lbl_superficie'] = 'Superficies';
$lang['lbl_procedimientos'] = 'Procedimientos';

$lang['lbl_config_avanzada_01'] = 'Selecciona procedimiento';
$lang['lbl_config_avanzada_02'] = 'Selecciona quirofano';
$lang['lbl_config_avanzada_03'] = 'Selecciona producto';
$lang['lbl_config_avanzada_04'] = 'Ver Configuración';


$lang['lbl_probabilidades'] = 'Ver probabilidades de LPP';
$lang['lbl_versuperficies'] = 'Ver superficies';
$lang['lbl_versuperficies_todas'] = 'Ver todas las superficies';

$lang['lbl_estaticas'] = 'Estáticas';
$lang['lbl_mixta'] = 'Mixtas';
$lang['lbl_dinamica'] = 'Dinámicas';

$lang['lbl_niveles'] = 'Niveles de Riesgo';

$lang['lbl_nivel_01'] = 'BAJO';
$lang['lbl_nivel_02'] = 'BAJO A MODERADO';
$lang['lbl_nivel_03'] = 'MODERADO';
$lang['lbl_nivel_04'] = 'ALTO';
$lang['lbl_nivel_05'] = 'MUY ALTO';

$lang['lbl_piel_01'] = 'Piel con lesión';
$lang['lbl_piel_02'] = 'Piel intacta';

$lang['lbl_riesgo_01'] = 'RIESGO Bajo';
$lang['lbl_riesgo_01_desc'] = 'Recomendamos superficies estáticas de alta calidad y de contenido variable.';

$lang['lbl_riesgo_02'] = 'RIESGO Medio';
$lang['lbl_riesgo_02_desc'] = 'Sugerimos superficies que sean dinámicas con una presión alternante o de presión constante';

$lang['lbl_riesgo_03'] = 'RIESGO Alto';
$lang['lbl_riesgo_03_desc'] = 'Sugerimos superficies que sean dinámicas con una presión alternante o de presión constante';
$lang['lbl_riesgos_txt'] = 'Superficie especializada indicada para el manejo de la presión:';

$lang['menu_nivel'] = 'Niveles';
$lang['menu_clasificacion'] = 'Asistencia para Superficies';

$lang['txt_sup_camas_descr'] = 'Elige la superficie perfecta de acuerdo al Nivel de Riesgo';

$lang['txt_niveles_titulo'] = 'Clasificaciones de Riesgo por Nivel';

/** LEVELES **/
$lang['lbl_bajo'] = 'BAJO';
$lang['lbl_medio'] = 'MEDIO';
$lang['lbl_alto'] = 'ALTO';

$lang['lbl_nombre_link_bajo'] = 'Ver superficies de riesgo bajo';
$lang['lbl_nombre_link_medio'] = 'Ver superficies de riesgo medio';
$lang['lbl_nombre_link_alto'] = 'Ver superficies de riesgo alto';

$lang['txt_t_01'] = 'RIESGO BAJO';
$lang['txt_d_01'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Para personas con un riesgo menor de desarrollar lesiones por presión, recomendamos utilizar superficies estáticas de alta calidad y de contenido variable como lo son el gel, la espuma y las viscoelásticas.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=bajo" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Ver superficies de riesgo bajo</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';
$lang['txt_t_02'] = 'RIESGO MEDIO';
$lang['txt_d_02'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Un paciente con un riesgo medio requiere de más cuidados y por consiguiente, de superficies que sean dinámicas con una presión alternante y, en el caso de que no sea posible este último tipo de superficie, se recomienda una de presión constante.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=medio" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Ver superficies de riesgo medio</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';

$lang['txt_t_03'] = 'RIESGO ALTO';
$lang['txt_d_03'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Un paciente con un riesgo alto requiere de más cuidados y por consiguiente, de superficies que sean dinámicas con una presión alternante y, en el caso de que no sea posible este último tipo de superficie, se recomienda una de presión constante.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=alto" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Ver superficies de riesgo alto</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';

/** ESCALA BRADEN **/

$lang['braden_titulo'] = 'Selecciona la variable correspondiente al paciente en cada categoría, para obtener como resultado el Nivel de Riesgo de desarrollar LPP.';

$lang['braden_titulo_02'] = 'ESCALA BRADEN';

$lang['braden_columna_titulo_01'] = 'PERCEPCIÓN SENSORIAL';
$lang['braden_columna_01_item_01'] = '1. Completamente limitada';
$lang['braden_columna_01_item_02'] = '2. Muy limitada';
$lang['braden_columna_01_item_03'] = '3. Ligeramente limitada';
$lang['braden_columna_01_item_04'] = '4. Sin limitaciones';

$lang['braden_columna_titulo_02'] = 'EXPOSICIÓN A LA HUMEDAD';
$lang['braden_columna_02_item_01'] = '1. Constantemente húmeda';
$lang['braden_columna_02_item_02'] = '2. A menudo húmeda';
$lang['braden_columna_02_item_03'] = '3. Ocasionalmente húmeda';
$lang['braden_columna_02_item_04'] = '4. Raramente húmeda';

$lang['braden_columna_titulo_03'] = 'ACTIVIDAD';
$lang['braden_columna_03_item_01'] = '1. Encamado';
$lang['braden_columna_03_item_02'] = '2. En silla';
$lang['braden_columna_03_item_03'] = '3. Deambula ocasionalmente';
$lang['braden_columna_03_item_04'] = '4. Deambula frecuentemente';

$lang['braden_columna_titulo_04'] = 'MOVILIDAD';
$lang['braden_columna_04_item_01'] = '1. Completamente inmóvil';
$lang['braden_columna_04_item_02'] = '2. Muy limitada';
$lang['braden_columna_04_item_03'] = '3. Ligeramente limitada';
$lang['braden_columna_04_item_04'] = '4. Sin limitaciones';

$lang['braden_columna_titulo_05'] = 'NUTRICIÓN';
$lang['braden_columna_05_item_01'] = '1. Muy pobre';
$lang['braden_columna_05_item_02'] = '2. Probablemente adecuada';
$lang['braden_columna_05_item_03'] = '3. Adecuada';
$lang['braden_columna_05_item_04'] = '4. Excelente';

$lang['braden_columna_titulo_06'] = 'ROCE Y PELIGRO DE LESIONES';
$lang['braden_columna_06_item_01'] = '1. Problema';
$lang['braden_columna_06_item_02'] = '2. Problema potencial';
$lang['braden_columna_06_item_03'] = '3. No existe problema';

$lang['braden_nivel'] = 'Nivel de riesgo:';
$lang['braden_desc'] = 'Puntos de corte: ≤16 riesgo bajo, ≤14 riesgo medio y ≤12 riesgo alto';

$lang['probabilidades_titulo'] = 'Probabilidad de desarrollar una LPP según la superficie de apoyo empleada:';
$lang['probabilidades_item_01'] = '21.8% <small>SUPERFICIE DE ESPUMA ESTÁNDAR</small>';
$lang['probabilidades_item_02'] = '8.9% <small>SUPERFICIE DE BAJA PRESIÓN CONSTANTE</small>';
$lang['probabilidades_item_03'] = '6.8% <small>SUPERFICIE DE PRESIÓN ALTERNANTE</small>';

$lang['algoritmo_home_titulo'] = 'Algoritmo de Asistencia para Superficies';
$lang['algoritmo_home_desc'] = 'basado en el Consenso y la Evidencia';
$lang['algoritmo_home_btn'] = 'Comenzar';
$lang['algoritmo_home_modal'] = 'Superficies especiales para el manejo de la presión (SEMP)<br><br>
		Según el documento XIII del GNEAUPP, las superficies de apoyo (SA) o superficies especiales para el manejo de la presión (SEMP) se definen como la superficie o dispositivo especializado, cuya configuración física y/o estructural permite la redistribución de la presión, así como otras funciones terapéuticas añadidas para el manejo de las cargas tisulares, fricción, cizalla y/o microclima, y que abarca el cuerpo de un individuo o una parte del mismo, según las diferentes posturas funcionales posibles.';

$lang['algoritmo_asistencia_titulo'] = '
		<h3 style="color:#fff">Presencia de riesgo de <div class="item-hints">
		  <div class="hint" data-position="4"><!-- is-hint -->
		    lesiones
		    <div class="hint-content do--split-children">
		      <p>Además de las superficies especiales, el documento de GNEAUPP no. XI 2008, ofrece una serie de cuidados a tener en cuenta como: la valoración de la piel, la movilización, incentivar el autocuidado del paciente, valorar el estado nutricional, realizar cambios posturales, aplicar protección local y vigilar la hidratación e higiene.</p>
		    </div>
		  </div>
		</div> por presión</h3>';

$lang['algoritmo_asistencia_bajada'] = 'Selecciona la variable correspondiente';
$lang['algoritmo_pin'] = 'Piel intacta';
$lang['algoritmo_pin_01'] = 'S&iacute;, est&aacute; en riesgo';
$lang['algoritmo_pin_02'] = 'No est&aacute; en riesgo';

$lang['algoritmo_pcl'] = 'Piel con lesión';

$lang['en_riesgo'] = 'En riesgo';

$lang['algoritmo_piel_titulo'] = '<h3 style="color:#fff;font-size: 40px;">Evaluación del riesgo de
				<div class="item-hints">
				  <div class="hint" data-position="4"><!-- is-hint -->
				    lesiones
				    <div class="hint-content do--split-children">
				      <p>Además de las superficies especiales, el documento de GNEAUPP no. XI 2008, ofrece una serie de cuidados a tener en cuenta como: la valoración de la piel, la movilización, incentivar el autocuidado del paciente, valorar el estado nutricional, realizar cambios posturales, aplicar protección local y vigilar la hidratación e higiene.</p>
				    </div>
				  </div>
				</div>
			 por presión</h3>';

$lang['algoritmo_piel_bajada'] = 'Selecciona el nivel de riesgo según la escala Braden';

$lang['algoritmo_piel_btn_braden'] = 'Ver escala Braden';

$lang['algoritmo_piel_riesgo_01'] = 'RIESGO BAJO';
$lang['algoritmo_piel_riesgo_desc_01'] = '<strong>Mayor de 15 puntos</strong> Escala Braden';

$lang['algoritmo_piel_riesgo_02'] = 'RIESGO MEDIO';
$lang['algoritmo_piel_riesgo_desc_02'] = '<strong>De 13 a 14 puntos</strong> Escala Braden';

$lang['algoritmo_piel_riesgo_03'] = 'RIESGO ALTO';
$lang['algoritmo_piel_riesgo_desc_03'] = '<strong>Menor de 12 puntos</strong> Escala Braden';

$lang['disclamer'] = 'Para más información consulta a tu especialista.<br>Las restricciones de configuración se aplican de acuerdo a los registros de cada país';

$lang['hlbax'] = 'Hillrom es parte de Baxter';


$lang['frase_01'] = 'Sabias que, en promedio, los hospitales tienen 20 quirófanos generales en los EE. UU., 6-7 en Latinoamérica y 12-14 en Europa';
$lang['frase_02'] = 'Dos tercios de los hospitales tienen quirófanos híbridos, entre 2 y 6 por hospital. Los quirófanos robóticos son menos frecuentes en LATAM';
$lang['frase_03'] = 'Los quirófanos generales se renuevan normalmente cada 5 años en EE. UU., Latinoamérica y Reino Unido';
$lang['frase_04'] = 'La calidad, la facilidad de uso, el impacto en la eficiencia y seguridad del flujo de trabajo y la relación calidad-precio son importantes a la hora de elegir productos.';
$lang['frase_05'] = 'Al comprar soluciones del quirófano, la calidad ocupa el primer lugar, seguida de la seguridad y luego la relación calidad-precio.';
$lang['frase_06'] = 'La mayoría de los hospitales tienen quirófanos generales donde los cirujanos y los directores de quirófano pasan la mayor parte de su tiempo.';
$lang['frase_07'] = 'Más de la mitad de los hospitales tienen quirófanos híbridos; los quirófanos robóticos son menos frecuentes.';
$lang['frase_08'] = 'La información del producto es de gran interés para los hospitales que buscan comprar nuevos equipos quirúrgicos.';


?>