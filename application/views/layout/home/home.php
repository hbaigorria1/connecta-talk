<section class="header-home d-flex align-items-center" style="background:url('<?=base_url()?>asset/img/header.jpg');">	
	<div class="absolute-connecta-logo">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<img src="<?=base_url()?>asset/img/logo-connecta.png" class="img-fluid logo-conecta">
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 content-header">
		<div class="container">
			<div class="row align-items-end">
				<div class="col-12 col-md-5">
					<h3>Lorem ipsum dolor sit amet</h3>
				</div>
				<div class="col-12 col-md-3">
					<a href="#" class="btn-header">ÚNETE AHORA</a>
				</div>
				<div class="col-12 col-md-4">
					<p>PRÓXIMAS JORNADAS</p>
					<h6>05<small>·</small>16<small>·</small>22</h6>
				</div>
			</div>
		</div>
	</div>
</section>