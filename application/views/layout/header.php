<?php $leng = $this->config->item('language_abbr');
        if ($leng == 'ar'){
          $this->lang->load('web_lang','spanish');
      $shortname = "es";
      $language = "spanish";
    }
    $tipos = $this->page_model->get_categories($language);
    if ($leng == 'br'){
      $this->lang->load('web_lang','portuguese');
      $shortname = "pt";
      $language = "portuguese";
    }

?>
<div class="header" class="w-100" style="text-align:left;">
  <div class="w-100" style=" background-color:#003399;">
    <div class="container">
      <div class="row m-0 w-100">
          <div class="col-6 logo d-inline-flex align-items-center justify-content-center" style="position:relative;">
          <span style="margin-right: auto;color:#fff; font-weight:bold; padding-left:0px;font-family:Arial;font-size:13px;"><?=$this->lang->line('hlbax')?></span>
          </div>
            <div class="col-6  d-flex align-items-center justify-content-center">
              <a href="<?=base_url()?>" style="display:inline-block;margin-left: auto;">
                  <img style="height:15px;" src="https://www.descubrehillrom.com/asset/img/LogoBaxter-01.png" class="logo-wexll">
              </a>
            </div> 
      </div>
    </div>
  </div>
</div>
