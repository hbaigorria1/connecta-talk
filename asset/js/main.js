// Attach scrollSpy to .wow elements for detect view exit events,
// then reset elements and add again for animation
/*
$('.wow').on('scrollSpy:exit', function() {
  $(this).css({
    'visibility': 'hidden',
    'animation-name': 'none'
  }).removeClass('animated');
  wow.addBox(this);
}).scrollSpy();*/


$(document).ready(function () {
  cartAction('','');
});

var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
let zoomer = function (){
  document.querySelector('#img-zoomer-box')
    .addEventListener('mousemove', function(e) {

    let original = document.querySelector('#img-1'),
        magnified = document.querySelector('#img-2'),
        style = magnified.style,
        x = e.pageX - this.offsetLeft,
        y = e.pageY - this.offsetTop,
        imgWidth = original.offsetWidth,
        imgHeight = original.offsetHeight,
        xperc = ((x/imgWidth) * 100),
        yperc = ((y/imgHeight) * 100);

    //lets user scroll past right edge of image
    if(x > (.01 * imgWidth)) {
      xperc += (.15 * xperc);
    };

    //lets user scroll past bottom edge of image
    if(y >= (.01 * imgHeight)) {
      yperc += (.15 * yperc);
    };

    style.backgroundPositionX = (xperc - 9) + '%';
    style.backgroundPositionY = (yperc - 9) + '%';

    style.left = (x - 180) + 'px';
    style.top = (y - 180) + 'px';

  }, false);
}();

$(window).load(function(){
   // PAGE IS FULLY LOADED  
   // FADE OUT YOUR OVERLAYING DIV
   $('#overlay').fadeOut();
});
$(document).ready(function(){
  // When page loads...:
  $("div.content div").hide(); // Hide all content
  
  /* Check for hashtag in url */
  if (window.location.hash.length>0) {
    console.log(window.location.hash);
    /*find the menu item with this hashtag*/
    $( "nav li a" ).each(function() {
      if ( $( this ).attr("href") == window.location.hash )
        $( this ).parent().addClass("current").show(); // Activate page in menu
    });
    $(window.location.hash).fadeIn(); // Fade in the active page content
  }
  else { /* no hashtag: */
    $("nav li:first").addClass("current").show(); // Activate first page
    $("div.content div:first").show(); // Show first page content
  }

  // On Click Event (within list-element!)
  $("nav li").click(function() {
    $("nav li").removeClass("current"); // Remove any active class
    $(this).addClass("current"); // Add "current" class to selected page
    
    $("div.content div").hide(); // Hide all content

    // Find the href attribute value to identify the active page:
    var activePage = $(this).find("a").attr("href"); 
    $(activePage).fadeIn(); // Fade in the active page
  }); // end click method
  
}); // end $(document).ready method

$(".cerrer-modal").click(function() {  
  $('.modal-product-formulario').hide();
});

$(".descargar").click(function() { 
  $('.modal-product-formulario').show();
});

$(".cerrer-video").click(function() {  
  $('.video-product-formulario').hide();
});

$(".video-click").click(function() { 
  $('.video-product-formulario').show();
});


$(".cerrer-video-v2").click(function() {  
  $('.video-product-formulario-v2').hide();
});

$(".video-click-v2").click(function() { 
  $('.video-product-formulario-v2').show();
});

$(".content-left-bottom-box").click(function() { 
  $('.modal-content-left-bottom').show();
  $(this).addClass('active-img');
});

$(".cerrar-modal-content-left-bottom").click(function() { 
  $('.modal-content-left-bottom').hide();
  $('.content-left-bottom-box').removeClass('active-img');
});

$(".content-right-bottom-box").click(function() { 
  $('.modal-content-right-bottom').show();
  $(this).addClass('active-img');
});

$(".cerrar-modal-content-right-bottom").click(function() { 
  $('.modal-content-right-bottom').hide();
  $('.content-right-bottom-box').removeClass('active-img');
});

$(".content-right-top-box").click(function() { 
  $('.modal-content-right-top').show();
  $(this).addClass('active-img');
});

$("#hamburger").click(function() { 
  $('#cart').removeClass('activecart');
});

$(".cerrar-modal-content-right-top").click(function() { 
  $('.modal-content-right-top').hide();
  $('.content-right-top-box').removeClass('active-img');
});

function showcart() {
   var element = document.getElementById("cart");
   element.classList.toggle("activecart");
}
$("#hamburger").toggle(
    function(){$(".menu-content").css({"background": "red"});},
    function(){$("p").css({"color": "blue"});},
    function(){$("p").css({"color": "green"});
  });



$('#gridproducts').owlCarousel({
    loop:false,
    margin:0,
    nav:true,
    autoplay: false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:8
        }
    }
})

function cartAction(action,product_id,posicion_id,op) {
var queryString = "";
  if(action != "") {
    switch(action) {
      case "add":
        queryString = 'action='+action+'&product_id='+ product_id+'&posicion_id='+ posicion_id+'&op='+ op;
      break;
      case "remove":
        queryString = 'action='+action+'&product_id='+ product_id+'&posicion_id='+ posicion_id;
      break;
      case "empty":
        queryString = 'action='+action;
      break;
    }  
  }

  jQuery.ajax({
  url: base_url+"/producto/add/",
  type: "POST",
  data:queryString,
  success:function(data){
    var res = jQuery.parseJSON(data);
    $("#cart").html(res.html);
    if(res.contador > 0){
      $(".contador").html(res.contador);
      $(".contador").show();
      $("#cart").show();
    } else {
      $(".contador").hide();
      $("#cart").hide('');
    }
    if(action != "") {
      switch(action) {

        case "add":
          $("#add_"+posicion_id).hide();
          $("#check_"+posicion_id).attr('checked', false);
          $("#added_"+posicion_id).show();
        break;
        case "remove":
          $("#add_"+posicion_id).show();
          $("#check_"+posicion_id).attr('checked', false);
          $("#added_"+posicion_id).hide();
        break;
        case "empty":
          $(".btnAddAction").show();
          $(".btnAdded").hide();
        break;
      }  
    }
  },
  error:function (){}
  }); 
}

